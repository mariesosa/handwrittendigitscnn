[![pylint_score](./images/pylint_badge.svg)](https://pylint.pycqa.org/en/latest/)
[![python docstring coverage](./images/interrogate_badge.svg)](https://interrogate.readthedocs.io/en/latest/)

# CNN for handwritten digits classification

A simple command-line program (written in Python) to load or train a
Convolutional Neural Network (CNN) dedicated to image classification of
handwritten digits. It uses the MNIST handwritten digits dataset included in
[Keras](https://github.com/keras-team/keras).

## Requirements

The script uses [Tensorflow](https://github.com/tensorflow/tensorflow) 2 (which
includes Keras). However, it could be used with older versions of Keras and
Tensorflow by modifying the imports like so:

    from tensorflow.keras import Sequential
    # becomes
    from keras import Sequential

It also required numpy, [h5py](https://github.com/h5py/h5py), graphviz, pydot
and matplotlib. See `requirements.txt` for information regarding package
versions.

The script is compatible with Python >= 3.6.

## Example use

Train the CNN and save the model to `cnn_model.h5`:

    cnn_handwritten_digits.py train --output-file cnn_model.h5

Load the saved model and evaluate it:

    cnn_handwritten_digits.py load --input-file cnn_model.h5

Argument `--show-plots` can be used to show plots of the first 9 incorrect
predictions:

    cnn_handwritten_digits.py --show-plots load --input-file cnn_model.h5
