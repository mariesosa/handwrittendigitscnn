#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Example use of CNN for image classification of handwritten digits."""

import warnings
import argparse

import numpy as np
import matplotlib.pyplot as plt

# Tensorflow/Keras
from tensorflow.keras.datasets.mnist import load_data
from tensorflow.keras import Sequential
import tensorflow.keras.layers as layers
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import plot_model

# Ignore future warnings from Tensorflow
warnings.simplefilter(action="ignore", category=FutureWarning)


def plot_image(predictions, true_label, image, class_names):
    """Plot the image, its predicted label and confidence and its true label.

    :param predictions: The array of prediction by class.
    :type predictions: numpy.ndarray of shape (n,)
    :param true_label: True class of the image.
    :type true_label: int
    :param image: The image to show (as an 3D array).
    :type image: numpy.ndarray
    :param class_name: The list of all class labels.
    :type class_name: list[int]
    """
    plt.xticks([])
    plt.yticks([])

    plt.imshow(image, cmap=plt.cm.binary)

    predicted_label = np.argmax(predictions)
    color = "blue" if predicted_label == true_label else "red"

    label = "{} {:.0f}% ({})".format(
        class_names[predicted_label], 100 * np.max(predictions), class_names[true_label]
    )
    plt.xlabel(label, color=color)


def plot_value_array(predictions, true_label):
    """Plot a bar plot representing the prediction confidence for all labels.

    :param predictions: The array of prediction by class.
    :type predictions: numpy.ndarray of shape (n,)
    :param true_label: True class of the image.
    :type true_label: int
    """
    plt.xticks(range(10))
    plt.yticks([])
    this_plot = plt.bar(range(10), predictions, color="#777777")
    plt.ylim([0, 1])

    # Set bar color to blue if the class prediction is correct, to red otherwise
    predicted_label = np.argmax(predictions)
    this_plot[predicted_label].set_color("red")
    this_plot[true_label].set_color("blue")


def define_cnn_model(in_shape, n_classes, model_file=None):
    """Define the CNN model using the sequential API.

    :param in_shape: The shape of the training images.
    :type in_shape: tuple[int]
    :param n_classes: The number of possible classes.
    :type n_classes: int
    :param model_file: If a string is given, save a png representation of the
        model under the given name using `tf.keras.utils.plot_model`.
    :type model_file: None or str
    :return: The created CNN model.
    :rtype: tf.keras.models.Sequential
    """
    model = Sequential()
    # Add a spatial convolution layer over images
    model.add(
        layers.Conv2D(
            32, (3, 3), activation="relu",
            kernel_initializer="he_uniform", input_shape=in_shape
        )
    )
    model.add(layers.MaxPool2D((2, 2)))
    # Flattens the input before the creation of the dense layer
    model.add(layers.Flatten())
    model.add(
        layers.Dense(100, activation="relu", kernel_initializer="he_uniform")
    )
    # Use the dropout layer to prevent overfitting. It randomly sets input units
    # to 0 with a given frequency (here 0.5) at each step during training time.
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(n_classes, activation="softmax"))

    # Create a png image representing the CNN as a graph
    if model_file:
        plot_model(model, model_file, show_shapes=True)

    return model


def main():
    """Main entry point with parser definition"""
    # Initialize the program parser
    description = "Use of CNN for image classification of handwritting numbers."
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "--show-plots",
        action="store_true",
        help="Show plots of first 9 incorrect predictions"
    )
    subparsers = parser.add_subparsers(dest="action")
    # Create sub-commands for the loading and the training actions
    load_parser = subparsers.add_parser(
        "load",
        help="Load a model from a file"
    )
    load_parser.add_argument(
        "--input-file",
        required=True,
        help="File path from which load the model"
    )
    train_parser = subparsers.add_parser(
        "train",
        help="Train the model and save it to a file"
    )
    train_parser.add_argument(
        "--output-file",
        required=True,
        help="File path in which saving the model"
    )
    args = parser.parse_args()

    # 1. Load the MNIST handwritten digits dataset
    (x_train, y_train), (x_test, y_test) = load_data()

    # 2. Pre-processing
    # Reshape the dataset: images are here arrays of grayscale data, but CNN
    # models expect images in a channels-last format, ind_plot.e. 3D representing
    # [rows, columns, channels]). Thus, we need to add a channel dimension to
    # the data in order to use it.
    x_train = x_train.reshape((x_train.shape[0], x_train.shape[1], x_train.shape[2], 1))
    x_test = x_test.reshape((x_test.shape[0], x_test.shape[1], x_test.shape[2], 1))

    # Get information regarding train/test sets size, the shape of the input
    # images and the number of classes.
    print("Dataset information:")
    print(f"Size of train set = {len(y_train)} - test set = {len(y_test)}")
    n_classes = len(set(y_train))
    in_shape = x_train.shape[1:]
    print(f"Number of classes = {n_classes}\nImage size = {in_shape}\n")

    # Normalize pixel values to range [0, 1] instead of [0, 255] in order to
    # speed up the learning process.
    x_train = x_train.astype("float32") / 255.
    x_test = x_test.astype("float32") / 255.

    # 3. Either load or train a CNN model
    if args.action == "train":
        # Train the model
        model = define_cnn_model(in_shape, n_classes, "cnn_model_graph.png")

        # Define the loss function and the optimization algorithm
        print("Compile the model...")
        model.compile(
            optimizer="adam",
            loss="sparse_categorical_crossentropy",
            metrics=["accuracy"]
        )

        # Fit and save the model
        print("Fit the model...\n")
        model.fit(x_train, y_train, epochs=10, batch_size=128, verbose=0)
        print(f"Save the CNN model to {args.output_file}\n")
        model.save(args.output_file)
    else:
        # Load the trained model
        print(f"Load the CNN model from {args.input_file}\n")
        model = load_model(args.input_file)

    # 4. Evaluate the model
    print("Evaluate the model...")
    _, accuracy = model.evaluate(x_test, y_test, verbose=2)
    print(f"Accuracy on the test set: {accuracy:.3f}\n")

    # Get predictions of the entire test set
    predictions = model.predict(x_test)

    # 5. Print and/or plot wrong predictions
    # Get incorrect predictions on this set
    wrong = [
        ind_plot for ind_plot in range(len(y_test))
        if y_test[ind_plot] != np.argmax(predictions[ind_plot])
    ]

    # Plot the first 9 images with incorrect prediction, their predicted
    # labels, and the true labels. Color correct predictions in blue and
    # incorrect predictions in red.
    if args.show_plots:
        num_rows = 3
        num_cols = 3
        class_names = list(range(10))
        title = f"First {num_rows * num_cols} incorrect predictions"
        num_images = num_rows * num_cols
        plt.figure(num=title, figsize=(2 * 2 * num_cols, 2 * num_rows))
        for ind_plot in range(num_images):
            ind_img = wrong[ind_plot]
            plt.subplot(num_rows, 2 * num_cols, 2 * ind_plot + 1)
            plot_image(
                predictions[ind_img], y_test[ind_img], x_test[ind_img], class_names
            )
            plt.subplot(num_rows, 2 * num_cols, 2 * ind_plot + 2)
            plot_value_array(predictions[ind_img], y_test[ind_img])
        plt.tight_layout()
        plt.show()

    # Print information regarding all images for which prediction was incorrect
    print("Wrong predictions on the test set:")
    for ind_img in wrong:
        ind_predict = predictions[ind_img]
        proba = 100 * np.max(ind_predict)
        print(
            "Image {} - predicted class = {} ({:.1f}%) - original = {}"
            .format(ind_img, np.argmax(ind_predict), proba, y_test[ind_img])
        )


if __name__ == "__main__":
    main()
